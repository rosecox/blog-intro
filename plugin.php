<?php
/**
 * Add an introduction to the site's blog page using the WordPress editor on the page_for_posts page.
 *
 * @package     BlogIntro
 * @author      Rose Cox
 * @copyright   2016 Purple Prodigy
 * @license     GPL-2.0+
 *
 * @wordpress-plugin
 * Plugin Name: Blog Intro Plugin
 * Plugin URI:  http://www.purpleprodigy.com
 * Description: Introduce your Blog Page - Add an introduction to the site's  page_for_posts page.
 * Version:     1.0.0
 * Author:      Rose Cox
 * Author URI:  http://www.purpleprodigy.com
 * Text Domain: blog_intro
 * License:     GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */

namespace BlogIntro;

if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Cheatin&#8217; uh?' );
}

require_once( __DIR__ . '/assets/vendor/autoload.php' );

add_action( 'init', __NAMESPACE__ . '\launch' );
function launch() {

}