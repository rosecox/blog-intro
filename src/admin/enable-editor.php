<?php
/**
 * Enable Editor feature for the Posts Page in the back-end.
 *
 * @package     BlogIntro
 * @since       1.0.0
 * @author      Purple Prodigy
 * @link        http://www.purpleprodigy.com
 * @licence     GNU General Public License 2.0+
 */
namespace BlogIntro;
