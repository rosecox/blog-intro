<?php
/**
 * Business Logic for front-end to grab contents out of the database,
 * prepare it for rendering and then call the view.
 *
 * @package     BlogIntro
 * @since       1.0.0
 * @author      Purple Prodigy
 * @link        http://www.purpleprodigy.com
 * @licence     GNU General Public License 2.0+
 */
namespace BlogIntro;
